<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//landing page

Route::get('/', 'LandingController@index');
Route::get('/artikel/{slug}', 'LandingController@article');
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/login', 'AuthController@loginPost');
Route::post('/logout', 'AuthController@getLogout')->name('logout');

Route::get('/warung-rakyat', 'LandingController@allWarung');
Route::get('/detail-warung/{id}', 'LandingController@detailWarung');
Route::get('/semua-artikel', 'LandingController@allArticle');




//admin page

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {


    Route::get('/dashboard', 'LandingController@dashboard');

    Route::resource('artikel', 'ArticlesController');
    Route::resource('tag', 'TagController');
    Route::resource('warung-rakyat', 'WarungController');

});
