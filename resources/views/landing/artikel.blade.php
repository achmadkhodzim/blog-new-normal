@extends('layouts.index')
@section('content')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('{{ $artikel->thumbnail }}');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
          <div class="col-md-9 ftco-animate pb-5 text-center">
            <h1 class="mb-3 bread">{{ $artikel->judul }}</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="/">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>{{ $artikel->judul }}</span></p>
          </div>
        </div>
      </div>
    </section>

		<section class="ftco-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 order-lg-last ftco-animate">
            
          {!! $artikel->isi !!}
          </div> <!-- .col-md-8 -->
          <div class="col-lg-4 sidebar pr-lg-5 ftco-animate">
            <div class="sidebar-box">
              <form action="/semua-artikel" method="GET" class="search-form">
                <div class="form-group">
                  <span class="icon icon-search"></span>
                  <input type="text" name="search" class="form-control" placeholder="Type a keyword and hit enter">
                </div>
              </form>
            </div>
            <!-- <div class="sidebar-box ftco-animate">
              <ul class="categories">
                <h3 class="heading mb-4">Categories</h3>
                <li><a href="#">Travel <span>(12)</span></a></li>
                <li><a href="#">Tour <span>(22)</span></a></li>
                <li><a href="#">Destination <span>(37)</span></a></li>
                <li><a href="#">Drinks <span>(42)</span></a></li>
                <li><a href="#">Foods <span>(14)</span></a></li>
                <li><a href="#">Travel <span>(140)</span></a></li>
              </ul>
            </div> -->

            <div class="sidebar-box ftco-animate">
              <h3 class="heading mb-4">Recent Blog</h3>
              @foreach($recent as $recent)
              @if($recent->slug != $artikel->slug)
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url({{ $recent -> thumbnail }});"></a>
                <div class="text">
                  <h3><a href="{{ '/artikel/'. $recent -> slug }}">{{$recent->judul }}</a></h3>
                  <div class="meta">
                    <div><a href="{{ '/artikel/'. $recent -> slug }}"><span class="icon-calendar"></span> {{ date('d M Y', strtotime($recent->created_at))  }}</a></div>
                    <div><a href="{{ '/artikel/'. $recent -> slug }}"><span class="icon-person"></span> Admin</a></div>

                  </div>
                </div>
              </div>
              @endif
              @endforeach
              
            </div>

            <div class="sidebar-box ftco-animate">
              <h3 class="heading mb-4">Tag</h3>
              <div class="tagcloud">
                <form action="/semua-artikel" method="GET">
                @foreach($tag as $tag)
                  <button style="cursor:pointer; background:none;" type="submit" name="tag" class="tag-cloud-link" value="{{$tag ->nama}}">{{$tag ->nama}}</button>
                @endforeach
              </form>
              </div>
            </div>

            <!-- <div class="sidebar-box ftco-animate">
              <h3 class="heading mb-4">Paragraph</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
            </div> -->
          </div>

        </div>
      </div>
    </section> <!-- .section -->
@endsection
