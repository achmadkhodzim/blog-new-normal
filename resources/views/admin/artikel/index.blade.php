@extends('layouts.admin')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Daftar Artikel</h1>
        <a
            href="/admin/artikel/create"
            class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"
            ><i class="fas fa-plus fa-sm text-white-50"></i> Buat Artikel
            Baru</a
        >
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Artikel</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table
                    class="table table-bordered"
                    id="dataTable"
                    width="100%"
                    cellspacing="0"
                >
                    <thead>
                        <tr>
                            <th>Judul</th>
                            <th>Tag</th>
                            <th>isi</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($articles as $article)
                        <tr>
                            <td>{{ $article -> judul }}</td>
                            <td>
                                @foreach($article -> tag as $tag)
                                    <p>{{ $tag -> nama }}</p>
                                @endforeach
                            </td>
                            <td>
                            @php
                            $isi = strip_tags($article->isi);
                            @endphp
                            {{ Str::words($isi, $words = 10, $end = '...') }}

                            <td class="d-flex">
                                <a
                                    href="{{ '/artikel/'.$article -> slug  }}"
                                    class="btn btn-dark btn-sm text-white mr-1"
                                >
                                    Lihat
                                </a>
                                <a
                                    href="{{ '/admin/artikel/'.$article -> slug.'/edit' }}"
                                    class="btn btn-primary btn-sm text-white mr-1"
                                >
                                    Edit
                                </a>
                                <form
                                    action="{{ '/admin/artikel/'.$article -> slug }}" method="POST" onclick="return confirm('Apakah kamu yakin ingin menghapus artikel ini?')"

                                >
                                 {{ csrf_field() }}
                                 {{ method_field('DELETE') }}

                                    <button class="btn btn-danger btn-sm text-white" type="submit">Hapus</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection

@push('css')
<link
    href="{{ asset('admin/vendor/datatables/dataTables.bootstrap4.min.css') }}"
    rel="stylesheet"
/>
@endpush

@push('javascript')
<script src="{{
        asset('admin/vendor/datatables/jquery.dataTables.min.js')
    }}"></script>
<script src="{{
        asset('admin/vendor/datatables/dataTables.bootstrap4.min.js')
    }}"></script>
<script src="{{ asset('admin/js/demo/datatables-demo.js') }}"></script>
@endpush
