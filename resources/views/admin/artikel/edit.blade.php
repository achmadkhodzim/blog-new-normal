@extends('layouts.admin')
@section('content')
<!-- Begin Page Content -->



<div class="container-fluid">

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Buat Artikel</h1>
        <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-book fa-sm text-white-50"></i> Buat Artikel
            Baru</a> -->
    </div>

    <div class="card shadow mb-4">
        <form action="{{ url('/admin/artikel/'.$artikel->slug) }}" method="POST" enctype="multipart/form-data"
            style="display:block">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Artikel</h6>
            </div>
            <div class="card-body">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" placeholder="judul"
                        value="{{ $artikel->judul }}">
                </div>

                <div class="form-group">
                    <label for="tag">Tag</label>
                    <select class="form-control select2" id="tag" name="tag[]" multiple="multiple">
                        @foreach($tags as $tag)
                        <option value="{{ $tag -> id }}" @foreach($artikel->tag as $selected)
                            @if( $tag -> id == $selected -> id)
                            selected
                            @endif
                            @endforeach
                            >{{ $tag -> nama }}</option>
                        @endforeach
                    </select>
                </div>


                <div class="form-group">
                    <label for="tag">Apakah dijadikan slider?</label>

                    @if($artikel -> slider == false)
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="slider" id="exampleRadios2" value="false"
                            checked>
                        <label class="form-check-label" for="exampleRadios2">
                            Tidak
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="slider" id="exampleRadios1" value="true">
                        <label class="form-check-label" for="exampleRadios1">
                            Iya
                        </label>
                    </div>

                    @else

                    <div class="form-check">
                        <input class="form-check-input" checked type="radio" name="slider" id="exampleRadios1" value="true">
                        <label class="form-check-label" for="exampleRadios1">
                            Iya
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="slider" id="exampleRadios2" value="false"
                            >
                        <label class="form-check-label" for="exampleRadios2">
                            Tidak
                        </label>
                    </div>

                    @endif

                </div>

                <div class="form-group">
                    <label for="thumbnail">thumbnail</label>
                    <input type="file" class="form-control" id="thumbnail" name="thumbnail"
                    data-height="300"
                        data-show-remove="false"
                        data-default-file="{{ $artikel -> thumbnail }}" value="{{ $artikel -> thumbnail }}">
                </div>
                <div class="form-group">
                    <label for="isi">Isi</label>
                    <textarea class="form-control summernote" id="isi" rows="5"
                        name="isi">{{ $artikel->isi }}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary mb-5">Simpan</button>
                </div>
            </div>

        </form>
    </div>
</div>
<!-- /.container-fluid -->
@endsection

@push('css')

@endpush

@push('javascript')

@endpush
