@extends('layouts.admin')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Warung Rakyat</h1>
        <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-book fa-sm text-white-50"></i> Edit Artikel
            Baru</a> -->
    </div>

    <div class="card shadow mb-4">
        <form action="{{ url('/admin/warung-rakyat/'.$shop->id) }}" method="POST" enctype="multipart/form-data"
            style="display:block">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Warung Rakyat</h6>
            </div>
            <div class="card-body">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label for="nama">Nama Toko</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="nama"
                        value="{{ $shop -> nama }}">
                </div>
                <div class="form-group">
                    <label for="nama">Nama Pemilik Toko</label>
                    <input type="text" class="form-control" id="nama" name="pemilik" placeholder="pemilik"
                        value="{{ $shop -> pemilik }}">
                </div>
                <div class="form-group">
                    <label for="nama">No. Hp Pemilik</label>
                    <input type="text" class="form-control" id="nama" name="nopemilik" placeholder="Nomor handphone"
                        value="{{ $shop -> nopemilik }}">
                </div>
                <div class="form-group">
                    <label for="nama">Metode Pengantaran</label>
                    <input type="text" class="form-control" id="nama" name="metode" placeholder="contoh: Ojek Online , COD"
                        value="{{ $shop -> metode }}">
                </div>
                <div class="form-group">
                    <label for="tag">Wilayah</label>
                    <select class="form-control" id="tag" name="wilayah">

                        @foreach($regions as $region)
                        <option value="{{ $region -> id }}">{{ $region -> name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class=" form-group">
                    <label for="isi">Alamat Detail Toko</label>
                    <textarea class="form-control summernote" id="isi" rows="5"
                        name="alamat">{{ $shop -> alamat }}</textarea>
                </div>
                <div class="form-group">
                    <label for="thumbnail">Photo</label>
                    <input
                        type="file"
                        class="form-control"
                        id="photo"
                        name="photo"
                        data-height="300"
                        data-show-remove="false"
                        data-default-file="{{ $shop -> photo }}"
                        >
                </div>

                <div class=" form-group">
                    <label for="isi">Deskripsi</label>
                    <textarea class="form-control summernote" id="isi" rows="5"
                        name="deskripsi">{{ $shop -> deskripsi }}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary mb-5">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /.container-fluid -->
@endsection

@push('css')

@endpush

@push('javascript')
<script>
    $('#photo').attr('value', '{{ $shop -> photo}}').dropify()
</script>

@endpush
