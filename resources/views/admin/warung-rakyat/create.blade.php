@extends('layouts.admin')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Buat Warung Rakyat</h1>
        <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-book fa-sm text-white-50"></i> Buat Artikel
            Baru</a> -->
    </div>

    <div class="card shadow mb-4">
        <form action="/admin/warung-rakyat" method="POST" enctype="multipart/form-data" style="display:block">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Warung Rakyat</h6>
            </div>
            <div class="card-body">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="nama">Nama Toko</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Toko">
                </div>
                <div class="form-group">
                    <label for="nama-pemilik">Nama Pemilik Toko</label>
                    <input type="text" class="form-control" id="nama-pemilik" name="pemilik" placeholder="Nama Pemilik">
                </div>
                <div class="form-group">
                    <label for="hp-pemilik">No. Hp Pemilik</label>
                    <input type="text" class="form-control" id="hp-pemilik" name="nopemilik" placeholder="Nomor Handphone">
                </div>
                <div class="form-group">
                    <label for="metode-pengantaran">Metode Pengantaran</label>
                    <input type="text" class="form-control" id="metode-pengantaran" name="metode" placeholder="contoh: Ojek Online , COD">
                </div>
                <div class="form-group">
                    <label for="tag">Wilayah</label>
                    <select class="form-control" id="tag" name="wilayah">
                        @foreach($regions as $region)
                        <option value="{{ $region -> id }}">{{ $region -> name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="alamat">Alamat Detail Toko</label>
                    <textarea class="form-control" id="alamat" rows="5" name="alamat"></textarea>
                </div>

                <div class="form-group">
                    <label for="thumbnail">Photo</label>
                    <input type="file" class="form-control" id="thumbnail" name="photo">
                </div>

                <div class="form-group">
                    <label for="isi">Deskripsi</label>
                    <textarea class="form-control summernote" id="isi" rows="5" name="deskripsi"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary mb-5">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /.container-fluid -->
@endsection

@push('css')

@endpush

@push('javascript')

@endpush
