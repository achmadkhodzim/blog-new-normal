@extends('layouts.admin')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Buat slide</h1>
        <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-book fa-sm text-white-50"></i> Buat slide
            Baru</a> -->
    </div>

    <div class="card shadow mb-4">
        <form action="/admin/featured-slides" method="POST" enctype="multipart/form-data" style="display:block">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">slide</h6>
            </div>
            <div class="card-body">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" placeholder="judul">
                </div>

                <div class="form-group">
                    <label for="thumbnail">Thumbnail</label>
                    <input type="file" class="form-control" id="thumbnail" name="thumbnail">
                </div>

                <div class="form-group">
                    <label for="isi">Deskripsi</label>
                    <textarea class="form-control summernote" id="deskripsi" rows="5" name="deskripsi"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary mb-5">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /.container-fluid -->
@endsection

@push('css')

@endpush

@push('javascript')

@endpush
