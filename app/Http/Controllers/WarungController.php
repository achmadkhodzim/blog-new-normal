<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Warung;
use App\Region;

class WarungController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shops = Warung::all();
        return view('admin.warung-rakyat.index', compact('shops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $regions = Region::where('regency_id','6404')->get();

        return view('admin.warung-rakyat.create',compact('regions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|max:255',
            'pemilik' => 'required|max:255',
            'wilayah' => 'required|max:255',
            'alamat' => 'required',
            'photo' => 'mimes:jpeg,jpg,png|required|max:10000',
            'nopemilik' => 'required|min:11|numeric',

          ]);

          if($request->hasFile('photo')){
            $resorce = $request->file('photo');
            $name = time().$resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/uploads", $name);
          }
    
          $warungrakyat = Warung::create([
            'nama' => $request->nama,
            'wilayah' => $request->wilayah,
            'alamat' => $request->alamat,
            'pemilik' => $request->pemilik,
            'metode' => $request->metode,
            'deskripsi' => $request->deskripsi,
            'nopemilik' => $request->nopemilik,

            'photo' => '/uploads/'.$name,
          ]);

          return redirect('/admin/warung-rakyat')->with('success','Tambah warung-rakyat Berhasil !');    
    }

    /**               

     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $shop = Warung::find($id);
        $regions = Region::where('regency_id','6404')->get();
        return view('admin.warung-rakyat.edit',compact('shop','regions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required|max:255',
            'pemilik' => 'required|max:255',
            'wilayah' => 'required|max:255',
            'alamat' => 'required',
            'nopemilik' => 'required|min:11|numeric',

          ]);

    
          if($request->hasFile('photo')){
            $resorce = $request->file('photo');
            $name = time().$resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/uploads", $name); 

            $post = [
              'nama' => $request->nama,
              'wilayah' => $request->wilayah,
              'alamat' => $request->alamat,
              'nopemilik' => $request->nopemilik,
              'photo' => '/uploads/'.$name,
              'pemilik' => $request->pemilik,
              'metode' => $request->metode,
              'deskripsi' => $request->deskripsi,
            ];
          }

          else{

            $post = [
              'nama' => $request->nama,
              'wilayah' => $request->wilayah,
              'alamat' => $request->alamat,
              'nopemilik' => $request->nopemilik,
              'pemilik' => $request->pemilik,
              'metode' => $request->metode,
              'deskripsi' => $request->deskripsi,
            ];
          }
 
 
          $shop = Warung::find($id);

          $shop->update($post);

          return redirect('/admin/warung-rakyat')->with('success','Edit warung-rakyat Berhasil !');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shop = Warung::find($id);
        $shop->delete();
        return redirect()->back()->with('success','Hapus warung-rakyat Berhasil !');
  
    }
}
