<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Tag;
use Illuminate\Support\Str;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        return view('admin.artikel.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        return view('admin.artikel.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'isi' => 'required',
            'thumbnail' => 'mimes:jpeg,jpg,png|required|max:10000',
          ]);

        if ($request->hasFile('thumbnail')) {
            $resorce = $request->file('thumbnail');
            $name = time().$resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/uploads", $name);
        }

        $artikel = Article::create([
            'judul' => $request->judul,
            'isi' => $request->isi,
            'slider' => $request->slider,
            'slug' => Str::slug($request->judul),
            'thumbnail' => '/uploads/'.$name,
          ]);

        $artikel->tag()->attach($request->tag);
        return redirect('/admin/artikel')->with('success', 'Tambah Artikel Berhasil !');
    }

    /**

     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $artikel)
    {
        $tags = Tag::all();
        return view('admin.artikel.edit', compact('tags', 'artikel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $artikel)
    {
        $this->validate($request, [
            'judul' => 'required',
            'isi' => 'required',
            'thumbnail' => 'mimes:jpeg,jpg,png|max:10000',
          ]);


        if ($request->hasFile('thumbnail')) {
            $resorce = $request->file('thumbnail');
            $name = time().$resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/uploads", $name);

            $post = [
              'judul' => $request->judul,
              'isi' => $request->isi,
              'slider' => $request->slider,
              'slug' => Str::slug($request->judul),
              'thumbnail' => '/uploads/'.$name,
            ];
        } else {
            $post = [
              'judul' => $request->judul,
              'isi' => $request->isi,
              'slider' => $request->slider,
              'slug' => Str::slug($request->judul),
            ];
        }




        $artikel->update($post);

        $artikel->tag()->sync($request->tag);
        return redirect('/admin/artikel')->with('success', 'Edit Artikel Berhasil !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $artikel)
    {
        $artikel->delete();
        return redirect()->back()->with('success', 'Hapus Artikel Berhasil !');
    }
}
