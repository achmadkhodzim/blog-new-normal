<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use Illuminate\Support\Str;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::all();
        return view('admin.slide.index', compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('admin.slide.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required|max:255',
            'deskripsi' => 'required',
            'thumbnail' => 'mimes:jpeg,jpg,png|required|max:10000',

          ]);

          if($request->hasFile('thumbnail')){
            $resorce = $request->file('thumbnail');
            $name = time().$resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/uploads", $name);
          }
    
          $slide = Slide::create([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'slug' => Str::slug($request->judul),
            'thumbnail' => '/uploads/'.$name,
          ]);

          return redirect('/admin/featured-slides')->with('success','Tambah Slide Berhasil !');    
    }

    /**               

     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = Slide::find($id);
        return view('admin.slide.edit',compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'judul' => 'required|max:255',
            'deskripsi' => 'required',
            'thumbnail' => 'mimes:jpeg,jpg,png|required|max:10000',

          ]);

          $slide = Slide::find($id);

          if($request->hasFile('thumbnail')){
            $resorce = $request->file('thumbnail');
            $name = time().$resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/uploads", $name);
            
          }

          $post = [
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'slug' => Str::slug($request->judul),
            'thumbnail' => '/uploads/'.$name,
          ];
 

          $slide->update($post);

          return redirect('/admin/featured-slides')->with('success','Edit Slide Berhasil !');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::find($id);
        $slide->delete();
        return redirect()->back()->with('success','Hapus Slide Berhasil !');
  
    }
}
