<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Slide;
use App\Warung;
use App\Tag;

class LandingController extends Controller
{
    public function index()
    {
      $tagfooter = Tag::paginate(5);

      $articles = Article::latest()->get();
      $slider = Article::where('slider','=','true')->get();
      return view('landing.index',compact('tagfooter','articles','slider'));
    }
    public function dashboard()
    {
      $articles = Article::all()->count();
      $shops = Warung::all()->count();
      return view('admin.index',compact('shops','articles'));
    }
    public function article($slug)
    {
      $tagfooter = Tag::paginate(5);

      $artikel = Article::where('slug','=',$slug)->first();
      Article::where('slug','=',$slug)->increment('view_count', 1);
      $recent = Article::latest()->paginate(4);
      $tag = Tag::all();
      return view('landing.artikel',compact('tagfooter','recent','tag','artikel'));
    }

    public function allWarung(Request $request)
    {
      $tagfooter = Tag::paginate(5);

      $search = null;

      if($request->has('search')){
        $search = $request->search;
        $warungs = Warung::where('nama','LIKE','%'.$search.'%')->paginate(5);
      }
      else {
        $warungs = Warung::paginate(5);
      }

      return view('landing.warung-rakyat',compact('tagfooter','warungs','search'));
    }

    public function detailWarung($id)
    {
      $warung = Warung::where('id','=',$id)->first();
      $tagfooter = Tag::paginate(5);
      return view('landing.detail-warung',compact('tagfooter','warung'));
    }

    public function allArticle(Request $request)
    {
      $tagfooter = Tag::paginate(5);

      $search = null;
      $tag = null;
      if($request->has('search')){
        $search = $request->search;
        $articles = Article::where('judul','LIKE','%'.$search.'%')->paginate(5);

      }
      else if($request->has('tag')){
        $tag = Tag::where('nama',$request->tag)->first();
        $articles = $tag->article()->paginate(5);
      }
      else {
        $articles = Article::paginate(5);
      }
      return view('landing.semua-artikel',compact('tagfooter','articles','search','tag'));
    }


}
