<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warung extends Model
{
    protected $guarded = [];

    public function region()
    {
        return $this->belongsTo('App\Region','wilayah');
    }   
}
