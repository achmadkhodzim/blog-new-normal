<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public function warung()
    {
        return $this->hasMany('App\Warung');
    }   
}
